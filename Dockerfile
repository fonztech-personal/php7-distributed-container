FROM nginx:1.21.4-alpine
ADD nginx-default.conf /etc/nginx/conf.d/default.conf
ADD php7.sh /etc/profile.d/php7.sh
ADD entrypoint.sh /root/entrypoint.sh
RUN apk update && \
  apk add php7-fpm php7-mcrypt php7-soap php7-openssl php7-gmp php7-pdo_odbc php7-json php7-dom php7-pdo php7-zip php7-mysqli php7-sqlite3 php7-apcu php7-pdo_pgsql php7-bcmath php7-gd php7-odbc php7-pdo_mysql php7-pdo_sqlite php7-gettext php7-xmlreader php7-xmlrpc php7-bz2 php7-iconv php7-pdo_dblib php7-curl php7-ctype tzdata && \
  chmod +x /etc/profile.d/php7.sh && \
  chmod +x /root/entrypoint.sh && \
  source /etc/profile.d/php7.sh && \
  adduser -D -g 'www' www && \
  mkdir /www && \
  chown -R www:www /www && \
  TIMEZONE="Europe/Rome" && \
  cp /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && \
  echo "${TIMEZONE}" > /etc/timezone && \
  sed -i "s|;*date.timezone =.*|date.timezone = ${TIMEZONE}|i" /etc/php7/php.ini
ENTRYPOINT [ "/bin/sh", "/root/entrypoint.sh" ]